# installing work env by choco

<p align="center">
<h1>This is the main scripts for setting up a desktop environment on Linux and Windows. Enjoy it </h1>
</p>

### Installation for Windows by [choco_install.ps1](https://github.com/greenblacked/seven/blob/master/choco_install.ps1 "choco_install.ps1")
First of all you need to downaload the script 
```ps1
wget "https://gitlab.com/greenblacked/installing-work-env-by-choco/-/raw/master/choco_install.ps1" -outfile "choco_install.ps1"
```
### And run it in powershell with admin access
```ps1
 .\choco_install.ps1
```

### After a successful setup you can select the next several options:
```ps1
cup all -y  # for update all of yours packages
choco uninstall $package_name # for delete the package
choco list --local-only # describe all installed package
choco install choco-package-list-backup # creating a backup list of all installed packages
choco install packages.config -y # another one tool for backups
```

