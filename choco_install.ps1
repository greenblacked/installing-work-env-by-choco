Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
cinst pester -version 2.0.2

choco install choco-package-list-backup -y #for backup app
choco install firefox -y
choco install chromium -y
choco install bitwarden -y
choco install 7zip -y
choco install vnc-viewer -y
choco install tightvnc -y
choco install doublecmd -y # file manager
choco install spotify -y
choco install wget -y
choco install postman -y
choco install notion -y
choco install mobaxterm -y
choco install drawio -y
choco install openssl.light -y
choco install choco-cleaner -y
choco install jq -y
choco install cyberduck -y
choco install dbeaver -y
choco install openvpn -y
choco install everything -y # search engine
choco install jre8 -y # java engine
choco install openjdk8 -y
choco install python -y
choco install powershell-core -y
choco install slack -y
choco install skype -y
choco install zoom -y
choco install telegram -y
choco install microsoft-teams.install -y
choco install git -y
choco install awscli -y
choco install azure-cli -y
choco install jdk8 -y
choco install notepadplusplus -y
choco install lightshot -y # take a customizable screenshot
choco install putty -y
choco install keepass -y
choco install sublimetext3 -y
choco install vscode -y
choco install vscode-icons -y
choco install vscode-powershell -y
choco install tortoisegit -y
choco install conemu -y
choco install adobereader -y
choco install pgadmin4 -y
choco install checksum -y # checksum validator


# choco install keepass-keepasshttp -y
# choco install keepassxc -y

# choco install unchecky -y # Unchecky aims to keep potentially unwanted programs out of your computer
# choco install googlechrome -y
# choco install unchecky -y # Unchecky aims to keep potentially unwanted programs out of your computer
# choco install libreoffice-fresh -y 
# choco install screenpresso -y # screen recorder
# choco install brackets -y
# choco install cmake -y
# choco install gimp -y
# choco install nodejs.install -y
# choco install python2 -y
# choco install paint.net -y
# choco install putty.portable -y
# choco install docker -y
choco list --local-only